import java.time.Instant
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

plugins {
    kotlin("jvm") version "1.4.30"
    id("com.google.cloud.tools.jib") version "2.7.1"
}

val group = "br.com.trustsystems"
val version = "0.3.0"

val tag = generateTag(version)

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib"))
    testImplementation("junit:junit:4.13.1")
}

jib {
    from {
        image = "bellsoft/liberica-openjdk-alpine-musl@sha256:01c623bf679aef626596e7850104e19adccd7f325fdcec2b0a8a41db261ca26a"
    }
    to {
        image = "registry.gitlab.com/wendersonferreira/ecdh"
        tags = setOf(tag)

        auth {
            username = System.getenv("REGISTRY_USER")
            password = System.getenv("REGISTRY_KEY")
        }
    }
    container {
        labels = mapOf(
            "maintainer" to "Trustsystems <wenderson@trustsystems.com.br>",
            "org.opencontainers.image.title" to "echd",
            "org.opencontainers.image.description" to "Ephemeral elliptic curve Diffie-Hellman key agreement in Java",
            "org.opencontainers.image.version" to tag,
            "org.opencontainers.image.authors" to "Wenderson Ferreira de Souza <wenderson@trustsystems.com.br>",
            "org.opencontainers.image.url" to "https://gitlab.com/wendersonferreira/ecdh",
            "org.opencontainers.image.vendor" to "https://trustsystems.com.br",
            "org.opencontainers.image.licenses" to "MIT"
        )
        creationTime = Instant.now().toString()
        jvmFlags = listOf(
            "-server",
            "-XX:+UseContainerSupport"
        )
        mainClass = "br.com.trustsystems.ecdh.AppKt"
    }
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(8))
        vendor.set(JvmVendorSpec.BELLSOFT)
    }
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    wrapper {
        gradleVersion = "6.8.1"
        distributionType = Wrapper.DistributionType.BIN
    }
}

fun generateTag(version: String): String {
    return when (version.substringAfterLast("-")) {
        "SNAPSHOT" ->
            version.replace("-SNAPSHOT", "")
                .plus("_").plus(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")))

        else -> version
    }
}

