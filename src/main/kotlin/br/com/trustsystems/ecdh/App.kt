package br.com.trustsystems.ecdh

import java.nio.ByteBuffer
import java.security.KeyFactory
import java.security.KeyPairGenerator
import java.security.MessageDigest
import java.security.spec.X509EncodedKeySpec
import javax.crypto.KeyAgreement
import javax.xml.bind.DatatypeConverter.parseHexBinary
import javax.xml.bind.DatatypeConverter.printHexBinary

fun main() {

    val console = System.console()
    val kpg = KeyPairGenerator.getInstance("EC")
    kpg.initialize(256)
    val kp = kpg.generateKeyPair()
    val ourPk = kp.public.encoded

    console.printf("Public Key: %s%n", printHexBinary(ourPk))

    val otherPk = parseHexBinary(console.readLine("Other PK: "))

    val kf = KeyFactory.getInstance("EC")
    val pkSpec = X509EncodedKeySpec(otherPk)
    val otherPublicKey = kf.generatePublic(pkSpec)

    val ka = KeyAgreement.getInstance("ECDH")
    ka.init(kp.private)
    ka.doPhase(otherPublicKey, true)

    val sharedSecret = ka.generateSecret()
    console.printf("Shared secret: %s%n", printHexBinary(sharedSecret))

    val hash = MessageDigest.getInstance("SHA-256")
    hash.update(sharedSecret)

    val keys = listOf(ByteBuffer.wrap(ourPk), ByteBuffer.wrap(otherPk))
    hash.update(keys[0])
    hash.update(keys[1])

    val derivedKey = hash.digest()
    console.printf("Final key: %s%n", printHexBinary(derivedKey))
}