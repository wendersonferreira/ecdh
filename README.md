<h1>Elliptic curve Diffie-Hellman key agreement</h1>

<h2>Objective</h2>

Show how to use java to discrete log DH and elliptic curve (ECDH). DH is a way for two parties to agree on a symmetric
secret key without explicitly communication that secret key. <b>this is only for demonstration purpose,
for one more efficient approach, check nacl</b> 

<h3>How to use<h3>

open two terminals, and execute this commands:
docker run docker run -it registry.gitlab.com/wendersonferreira/ecdh:${tag}, this moment your public key 
will be calculated copy this public key and put in the other terminal, press enter, your shared secret will generated

example:

TERMINAL-1

Public Key: 3059301306072A8648CE3D020106082A8648CE3D030107034200047DAEFF4ECCDD7459A060A80ED9FC6A71603068211FCC574D9C6A274623D619347E46EF14EB77FC9DBA2AA1D107CB674BC3CE79191A83330427A15F75F4EE300E

Other PK: 3059301306072A8648CE3D020106082A8648CE3D03010703420004F4E39458F5C989C759D5B2D7A7840E7FA439D79BCFDAEFA3B7F0467E231C9BAF57F798F41FE0BC3F5E8A97784DDA567740B45E6FFA5354F150221C9B80C64152

TERMINAL-2 

Public Key: 3059301306072A8648CE3D020106082A8648CE3D03010703420004F4E39458F5C989C759D5B2D7A7840E7FA439D79BCFDAEFA3B7F0467E231C9BAF57F798F41FE0BC3F5E8A97784DDA567740B45E6FFA5354F150221C9B80C64152

Other PK: 3059301306072A8648CE3D020106082A8648CE3D030107034200047DAEFF4ECCDD7459A060A80ED9FC6A71603068211FCC574D9C6A274623D619347E46EF14EB77FC9DBA2AA1D107CB674BC3CE79191A83330427A15F75F4EE300E

Result:

Shared secret: D4F632539617AC109975634FE7B3CF71EAC6F3F11BC1E74870A82264C2113175

Final key: 43D4C2E2994085327D3AF8ADFDFF708F084117B9B2B041AEB62E73EA93358AAA
<h3>CI/CD<h3>

I'm using gitlab ci/cd and jib gradle plugin: This plugin was designed to reuse docker approach of layers
turning your delivery more fast and more thinner. The mean objective is avoid create a fat jar. 

layers:

* dependencies -> changes rarely 
* resources -> changes from time to time
* code -> changes frequently


based on it Docker reuses the layers of OS and JRE. So we save storage in the Docker registry and speed
up the upload to and download from the registry because only fews MB will be transferred.

if you are logged in your registry, you can call 

docker login --username=yourhubusername --email=youremail@company.com
    
./gradlew build test jib

that is it, your imagem will be build and delivered to your registry

License
---
MIT


**Trustsystems, Wenderson 2020**